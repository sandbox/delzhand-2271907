(function ($) {

Drupal.behaviors.image_pinterest = {
  attach: function(context, settings) {
    $('img[data-pin="true"]').after(function(){
      return '<a href="//www.pinterest.com/pin/create/button/?url=' + $(this).attr('src') + '" data-pin-do="buttonPin" data-pin-config="none" data-pin-color="red"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png" /></a>'
    });
  }
}

})(jQuery);
